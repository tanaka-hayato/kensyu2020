-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.11-MariaDB
-- PHP のバージョン: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `kensyu _tanaka`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `test`
--

CREATE TABLE `test` (
  `ID` int(10) NOT NULL,
  `dish_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `janru` varchar(30) COLLATE utf8_bin NOT NULL,
  `price` int(10) NOT NULL,
  `memo` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `test`
--

INSERT INTO `test` (`ID`, `dish_name`, `janru`, `price`, `memo`) VALUES
(1, '豚骨ラーメン', 'ラーメン', 850, '餃子定食、替え玉'),
(2, '焼肉定食', '肉', 950, '牛タン、カルビ、ロース、ハラミの中から2種類'),
(3, 'スパゲッティ', 'イタリア', 740, 'クリーム、ケチャップのどちらか'),
(4, '蕎麦', '日本食', 700, 'アレルギー注意'),
(5, '蒙古タンメン中本', 'ラーメン', 800, '辛さを選べます'),
(6, '焼き鳥', '肉', 100, '2本で１セット'),
(7, '寿司', '日本食', 100, '一皿100円'),
(8, 'ピザ', 'イタリア', 900, '種類が豊富'),
(9, '豚の生姜焼き', '肉', 700, NULL),
(10, 'ショートケーキ', 'スイーツ', 450, 'イチゴ');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`ID`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `test`
--
ALTER TABLE `test`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
