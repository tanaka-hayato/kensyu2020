<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  include("./include/statics.php");
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_hayato; charset=utf8";
  $DB_USER = "tanaka_hayato";
  $DB_PW = "1234";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  $query_str = "SELECT *
                     FROM member AS m
                LEfT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                WHERE `member_ID` = " . $_GET['member_ID'] ;


  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>
  <html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員名簿システム 編集ページ</title>
  </head>
  <body>

    <h1>社員名簿システム</h1>
    <a href="index.php">トップページ</a>
    <a href="entry01.php">新入社員登録</a>
    <hr>
   <form method="post" action="entry_update02.php">
    <table border="2" style="border-collapse:collapse;">
     <tr>
       <th><strong>社員ID</strong></th>
       <td><input type= "hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>" ><?php echo $result[0]['member_ID']; ?></td>
     </tr>
        <th><strong>名前</strong></th>
        <td><input type="text" name="onamae" value="<?php echo $result[0]['name']; ?>"></td>
     </tr>
     <tr>
      <th><strong>出身地</strong></th>
      <td><select name="pref">
            <?php
              foreach ($pref_array AS $key => $value) {
                echo "<option value='" . $key . "'" ;
                if($result[0]['pref'] == $key){ echo " selected ";}
                echo ">" . $value . "</option>";
              }
            ?>
            </option>

          </select></td>
     </tr>
     <tr>
      <th><strong>性別</strong></th>
      <td><input type="radio" name="seibetu" value="1" <?php if($result[0]['seibetu'] == '1'){echo "checked";}?>/>男性
          <input type="radio" name="seibetu" value="2" <?php if($result[0]['seibetu'] == '2'){echo "checked";}?> />女性
      </td>
     </tr>
     <tr>
      <th><strong>年齢</strong></th>
      <td><input type= "text" name="age" value="<?php echo $result[0]['age']; ?>">歳</td>
     </tr>
     <tr>
      <th><strong>所属部署</strong></th>
      <td><input type="radio" name="section" value="1" <?php if($result[0]['section_ID'] == '1'){echo "checked";}?>/>第一事業部
          <input type="radio" name="section" value="2" <?php if($result[0]['section_ID'] == '2'){echo "checked";}?>/>第二事業部
          <input type="radio" name="section" value="3" <?php if($result[0]['section_ID'] == '3'){echo "checked";}?>/>営業
          <input type="radio" name="section" value="4" <?php if($result[0]['section_ID'] == '4'){echo "checked";}?>/>総務
          <input type="radio" name="section" value="5" <?php if($result[0]['section_ID'] == '5'){echo "checked";}?>/>人事
      </td>
    </tr>
    <tr>
     <th><strong>役職</strong></th>
     <td><input type="radio" name="grade" value="1" <?php if($result[0]['grade_ID'] == '1'){echo "checked";}?>/>営業部長
         <input type="radio" name="grade" value="2" <?php if($result[0]['grade_ID'] == '2'){echo "checked";}?>/>部長
         <input type="radio" name="grade" value="3" <?php if($result[0]['grade_ID'] == '3'){echo "checked";}?>/>チームリーダー
         <input type="radio" name="grade" value="4" <?php if($result[0]['grade_ID'] == '4'){echo "checked";}?>/>リーダー
         <input type="radio" name="grade" value="5" <?php if($result[0]['grade_ID'] == '5'){echo "checked";}?>/>メンバー
     </td>
   </tr>
   <tr>
     <td colspan="1" style="text-align:Center;">
     <td><input type="submit"><input type="reset"></td>
   </tr>
 </table>



   </form>
  </body>
</html>
