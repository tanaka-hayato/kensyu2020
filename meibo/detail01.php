<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  include("./include/statics.php");
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_hayato; charset=utf8";
  $DB_USER = "tanaka_hayato";
  $DB_PW = "1234";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $query_str = "SELECT m.member_ID AS
  //                      ID, m.name AS name,
  //                      gm.grade_name AS grade,
  //                      sm.section_name AS section
  //                      FROM member AS m
  //               LEfT JOIN grade_master AS gm ON gm.ID = m.grade_ID
  //               LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
  //               WHERE `member_ID` = " . $_GET['member_ID'] ;
    $query_str = "SELECT *
                       FROM member AS m
                  LEfT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                  LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                  WHERE `member_ID` = " . $_GET['member_ID'] ;


  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  ?>
  <html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員名簿システム 詳細ページ</title>
  </head>
  <body>

    <h1>社員名簿システム</h1>
    <a href="index.php" >トップページ</a>
    <a href="entry01.php">新入社員登録</a>
    <hr>
    <table border="2" style="border-collapse:collapse;">
     <tr>
        <th><strong>名前</strong></th>
        <td><?php echo $result[0]['name']; ?></td>
     </tr>
     <tr>
      <th><strong>出身地</strong></th>
        <td><?php echo $pref_array[$result[0]['pref']]; ?></td>
     </tr>
     <tr>
      <th><strong>性別</strong></th>
      <td><?php echo $gender_array[$result[0]['seibetu']]; ?></td>
     </tr>
     <tr>
      <th><strong>年齢</strong></th>
       <td><?php echo $result[0]['age']; ?></td>
     </tr>
     <tr>
      <th><strong>所属部署</strong></th>
      <td><?php  echo $result[0]['section_name']?></td>
    </tr>
    <tr>
     <th><strong>役職</strong></th>
      <td><?php  echo $result[0]['grade_name']?></td>
     </td>
   </tr>
   <tr>
     <th colspan="1" style="border-collapse:collapse;">
       <td>
       <form method="get" action="entry_update01.php">
        <input type="hidden" name="member_ID" value="<?php echo $_GET['member_ID'];?>">
        <input type="submit" value="編集" >
       </form>
       <form method="post" action="delete.php">
        <input type="hidden" name="member_ID" value="<?php echo $_GET['member_ID'];?>">
        <input type="submit" value="削除">
      </form>
    </td>
    </th>

   </tr>
 </table>
  <pre>
 <?php

 //var_dump($result);

// ?>

</pre>
   </form>
  </body>
</html>
