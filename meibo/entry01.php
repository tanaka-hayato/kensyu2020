<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  include("./include/statics.php");
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_hayato; charset=utf8";
  $DB_USER = "tanaka_hayato";
  $DB_PW = "1234";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  ?>
  <html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員名簿システム 登録ページ</title>
  </head>
  <body>

    <h1>社員名簿システム</h1>
    <a href="index.php">トップページ</a>
    <a href="entry01.php">新入社員登録</a>
    <hr>
   <form method="post" action="entry02.php">
    <table border="2" style="border-collapse:collapse;">
     <tr>
        <th><strong>名前</strong></th>
        <td><input type= "text" name="onamae" ></td>
     </tr>
     <tr>
      <th><strong>出身地</strong></th>
      <td><select name="pref">
            <option value="" selected>都道府県</option>
            <?php
              foreach ($pref_array AS $key => $value) {
              echo "<option value='" . $key . "'>" . $value . "</option>";
            } ?>
          </select></td>
     </tr>
     <tr>
      <th><strong>性別</strong></th>
      <td><input type="radio" name="seibetu" value="1" />男性
          <input type="radio" name="seibetu" value="2" />女性
      </td>
     </tr>
     <tr>
      <th><strong>年齢</strong></th>
      <td><input type= "text" name="age" >歳</td>
     </tr>
     <tr>
      <th><strong>所属部署</strong></th>
      <td><input type="radio" name="section" value="1" />第一事業部
          <input type="radio" name="section" value="2" />第二事業部
          <input type="radio" name="section" value="3" />営業
          <input type="radio" name="section" value="4" />総務
          <input type="radio" name="section" value="5" />人事
      </td>
    </tr>
    <tr>
     <th><strong>役職</strong></th>
     <td><input type="radio" name="grade" value="1" />営業部長
         <input type="radio" name="grade" value="2" />部長
         <input type="radio" name="grade" value="3" />チームリーダー
         <input type="radio" name="grade" value="4" />リーダー
         <input type="radio" name="grade" value="5" />メンバー
     </td>
   </tr>
   <tr>
     <th colspan="1" style="text-align:Center;"></th>
     <td><input type="submit" value="登録"><input type="reset"></td>
   </tr>
 </table>
   </form>
  </body>
</html>
