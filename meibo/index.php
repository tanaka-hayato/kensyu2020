<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  include("./include/statics.php");
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_hayato; charset=utf8";
  $DB_USER = "tanaka_hayato";
  $DB_PW = "1234";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT m.member_ID AS
                       ID, m.name AS name,
                       m.seibetu AS seibetu ,
                       gm.grade_name AS grade,
                       sm.section_name AS section
                FROM member AS m
                 LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                 LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                 where 1=1";
   if(isset($_GET['onamae']) AND $_GET['onamae'] != ""){
     $query_str .= " AND m.name LIKE '%" . $_GET['onamae'] . "%' ";
   }

   if(isset($_GET['seibetu']) AND $_GET['seibetu'] != ""){
     $query_str .= " AND m.seibetu = ". $_GET['seibetu'] ;
   }
   if(isset($_GET['grade']) AND $_GET['grade'] != ""){
    $query_str .= " AND m.grade_ID  = ". $_GET['grade'];
   }
   if(isset($_GET['section']) AND $_GET['section'] != ""){
     $query_str .= " AND m.section_ID  = " . $_GET['section'];
   }

  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

  ?>
  <html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>社員名簿システム</title>
  </head>
  <body>
    <h1>社員名簿システム</h1>
    <a href="index.php">トップページ</a>
    <a href="entry01.php">新入社員登録</a>
    <hr>
  <form method="GET" action="index.php">


         <th>名前:</th>
         <input type= "text" name="onamae">
         <th>性別:</th>
         <td><select name="seibetu">
               <option value="" selected="selected">すべて</option>
               <option value="1">男性</option>
               <option value="2">女性</option>
             </select></td>

         <th>部署:</th>
          <td><select name="section">
                   <option value="" selected="section">すべて</option>
                   <option value="1">第一事業部</option>
                   <option value="2">第二事業部</option>
                   <option value="3">営業</option>
                   <option value="4">総務</option>
                   <option value="5">人事</option>
                 </select></td>
         <th>役職:</th>
          <td><select name="grade">
                   <option value="" selected="grade">すべて</option>
                   <option value="1">事業部長</option>
                   <option value="2">部長</option>
                   <option value="3">チームリーダー</option>
                   <option value="4">リーダー</option>
                   <option value="5">メンバー</option>
              </select></td>
    <table>
          <tr>
          <td colspan="2" style="text-align:right;">
            <input type="submit"><input type="reset"></td>
        </tr>
    </table>
     <hr>
    <table border="1" style="border-collapse:collapse;">
        <tr>
           <th>メンバーID</th>
           <th>名前</th>
           <th>性別</th>
           <th>所属部署</th>
           <th>役職</th>

        </tr>


    <?php
     foreach ($result as $each){

      echo  "<tr><td>" . $each['ID'] ."</td>"
            ."<td><a href='detail01.php?member_ID=". $each['ID'] ."'>"
            . $each['name'] ."</a></td>"
            ."<td>" . $gender_array[$each['seibetu']] ."</td>"
            ."<td>" . $each['section'] ."</td>"
            ."<td>" . $each['grade'] ."</td></tr>";

        }
    ?>
  </table>
  </form>
   
  </body>

  </html>
