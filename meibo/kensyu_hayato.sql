-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.11-MariaDB
-- PHP のバージョン: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `kensyu_hayato`
--
CREATE DATABASE IF NOT EXISTS `kensyu_hayato` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `kensyu_hayato`;

-- --------------------------------------------------------

--
-- テーブルの構造 `grade_master`
--

CREATE TABLE `grade_master` (
  `ID` int(11) NOT NULL,
  `grade_name` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `grade_master`
--

INSERT INTO `grade_master` (`ID`, `grade_name`) VALUES
(1, '事業部長'),
(2, '部長'),
(3, 'チームリーダー'),
(4, 'リーダー'),
(5, 'メンバー');

-- --------------------------------------------------------

--
-- テーブルの構造 `member`
--

CREATE TABLE `member` (
  `member_ID` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `pref` varchar(100) CHARACTER SET utf8 NOT NULL,
  `seibetu` varchar(10) CHARACTER SET utf8 NOT NULL,
  `age` tinyint(5) NOT NULL,
  `section_ID` int(11) NOT NULL,
  `grade_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `member`
--

INSERT INTO `member` (`member_ID`, `name`, `pref`, `seibetu`, `age`, `section_ID`, `grade_ID`) VALUES
(1, '斉藤和巳', '6', '1', 25, 5, 1),
(2, '城島健司', '42', '1', 27, 4, 2),
(3, '松中信彦', '10', '1', 30, 3, 5),
(4, '井口資仁', '1', '2', 30, 1, 3),
(5, '小久保裕紀', '12', '2', 27, 5, 4),
(6, '城島健司', '1', '2', 4, 3, 3),
(7, 'グラシアル', '30', '1', 28, 2, 1),
(8, '柳田悠岐', '17', '2', 25, 1, 4),
(9, '秋山幸二', '32', '1', 30, 3, 3),
(10, '王貞治　', '25', '1', 35, 4, 2),
(18, '野村克也', '2', '1', 43, 1, 1),
(22, '上林誠知', '11', '1', 26, 1, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `section1_master`
--

CREATE TABLE `section1_master` (
  `ID` int(11) NOT NULL,
  `section_name` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `section1_master`
--

INSERT INTO `section1_master` (`ID`, `section_name`) VALUES
(1, '第一事業部'),
(2, '第二事業部'),
(3, '営業'),
(4, '総務'),
(5, '人事');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `grade_master`
--
ALTER TABLE `grade_master`
  ADD PRIMARY KEY (`ID`);

--
-- テーブルのインデックス `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_ID`) USING BTREE;

--
-- テーブルのインデックス `section1_master`
--
ALTER TABLE `section1_master`
  ADD PRIMARY KEY (`ID`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `grade_master`
--
ALTER TABLE `grade_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- テーブルのAUTO_INCREMENT `member`
--
ALTER TABLE `member`
  MODIFY `member_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- テーブルのAUTO_INCREMENT `section1_master`
--
ALTER TABLE `section1_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
