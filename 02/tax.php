<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form method="post" action="tax.php">
     <table border="1">
      <tr>
        <th>商品名</th>
        <th>価格（単位：円、税抜き）</th>
        <th>個数</th>
        <th>税率</th>
      </tr>
      <tr>
        <td><input type= "text" name="syouhinmei01" ></td>
        <td style="text-align:right"><input type="text"  name="kakaku01">円</td>
        <td style="text-align:right"><input type="text"  name="kosuu01">個</td>
        <td style="text-align:right"><input type="radio" name="zeiritu01" value="0.08" />8%
            <input type="radio" name="zeiritu01" value="0.10" />10%</td>
      </tr>
      <tr>
        <td><input type= "text" name="syouhinmei02" ></td>
        <td style="text-align:right"><input type="text"  name="kakaku02">円</td>
        <td style="text-align:right"><input type="text"  name="kosuu02">個</td>
        <td><input type="radio" name="zeiritu02" value="0.08" />8%
            <input type="radio" name="zeiritu02" value="0.10" />10%</td>
      </tr>
      <tr>
        <td><input type= "text" name="syouhinmei03" ></td>
        <td style="text-align:right"><input type="text"  name="kakaku03">円</td>
        <td style="text-align:right"><input type="text"  name="kosuu03">個</td>
        <td><input type="radio" name="zeiritu03" value="0.08" />8%
            <input type="radio" name="zeiritu03" value="0.10" />10%</td>
      </tr>
      <tr>
        <td><input type= "text" name="syouhinmei04" ></td>
        <td style="text-align:right"><input type="text"  name="kakaku04">円</td>
        <td style="text-align:right"><input type="text"  name="kosuu04">個</td>
        <td><input type="radio" name="zeiritu04" value="0.08" />8%
            <input type="radio" name="zeiritu04" value="0.10" />10%</td>
      </tr>
      <tr>
        <td><input type= "text" name="syouhinmei05" ></td>
        <td style="text-align:right"><input type="text"  name="kakaku05">円</td>
        <td style="text-align:right"><input type="text"  name="kosuu05">個</td>
        <td><input type="radio" name="zeiritu05" value="0.08" />8%
            <input type="radio" name="zeiritu05" value="0.10" />10%</td>
      </tr>
      <td><input type="submit"><input type="reset"></td>

     </table>
     <?php
       $total= $_POST['kakaku01']*$_POST['kosuu01']*(1+$_POST['zeiritu01']);

       $total= $total +($_POST['kakaku02']*$_POST['kosuu02']*(1+$_POST['zeiritu02']));

       $total= $total +($_POST['kakaku03']*$_POST['kosuu03']*(1+$_POST['zeiritu03']));

       $total= $total +($_POST['kakaku04']*$_POST['kosuu04']*(1+$_POST['zeiritu04']));

       $total= $total +($_POST['kakaku05']*$_POST['kosuu05']*(1+$_POST['zeiritu05']));

     ?>

      <pre>
  </form>
   <table border="2">
     <tr>
       <th>商品名</th>
       <th>価格（単位：円 税抜き）</th>
       <th>個数</th>
       <th>税率</th>
       <th>小計(単位：円)</th>
     </tr>
     <tr>
       <td width="30" height="10"><?php echo $_POST['syouhinmei01']; ?></td>
       <td style="text-align:right"><?php echo $_POST['kakaku01']; ?>円</td>
       <td style="text-align:right"><?php echo $_POST['kosuu01']; ?>個</td>
       <td style="text-align:right"><?php echo $_POST['zeiritu01']; ?>％</td>
       <td style="text-align:right"><?php echo number_format($_POST['kakaku01']*$_POST['kosuu01']*(1+$_POST['zeiritu01'])); ?>円</td>
     </tr>
     <tr>
       <td width="30" height="10"><?php echo $_POST['syouhinmei02']; ?></td>
       <td style="text-align:right"><?php echo $_POST['kakaku02']; ?>円</td>
       <td style="text-align:right"><?php echo $_POST['kosuu02']; ?>個</td>
       <td style="text-align:right"><?php echo $_POST['zeiritu02']; ?>％</td>
       <td style="text-align:right"><?php echo number_format($_POST['kakaku02']*$_POST['kosuu02']*(1+$_POST['zeiritu02'])); ?>円</td>
     </tr>
     <tr>
       <td width="30" height="10"><?php echo $_POST['syouhinmei03']; ?></td>
       <td style="text-align:right"><?php echo $_POST['kakaku03']; ?>円</td>
       <td style="text-align:right"><?php echo $_POST['kosuu03']; ?>個</td>
       <td style="text-align:right"><?php echo $_POST['zeiritu03']; ?>％</td>
       <td style="text-align:right"><?php echo number_format($_POST['kakaku03']*$_POST['kosuu03']*(1+$_POST['zeiritu03'])); ?>円</td>
     </tr>
       <td width="30" height="10"><?php echo $_POST['syouhinmei04']; ?></td>
       <td style="text-align:right"><?php echo $_POST['kakaku04']; ?>円</td>
       <td style="text-align:right"><?php echo $_POST['kosuu04']; ?>個</td>
       <td style="text-align:right"><?php echo $_POST['zeiritu04']; ?>％</td>
       <td style="text-align:right"><?php echo number_format($_POST['kakaku04']*$_POST['kosuu04']*(1+$_POST['zeiritu04'])); ?>円</td>
     <tr>

     </tr>
     <tr>
       <td width="30" height="10"><?php echo $_POST['syouhinmei05']; ?></td>
       <td style="text-align:right"><?php echo $_POST['kakaku05']; ?>円</td>
       <td style="text-align:right"><?php echo $_POST['kosuu05']; ?>個</td>
       <td style="text-align:right"><?php echo $_POST['zeiritu05']; ?>％</td>
       <td style="text-align:right"><?php echo number_format($_POST['kakaku05']*$_POST['kosuu05']*(1+$_POST['zeiritu05'])); ?>円</td>
     </tr>
     <tr>
       <th>合計金額</th>
       <td style="text-align:right"  colspan="4"><?php echo number_format($total);?>円</td>
    </tr>
 </table>

   <?php
       //  $_POST["syouhinnmei"];
      echo $_POST['syouhinmei01'] . "は";
      echo $_POST['kakaku01'] ."円で";
      echo $_POST['kosuu01']."個で";
      echo $_POST['zeiritu01']."%です。";
     $goukei=$_POST['kakaku01']*$_POST['kosuu01']*(1+$_POST['zeiritu01']);
      echo "合計金額" .$goukei. "です。"


   ?>

    <pre>


   <?php

   var_dump($_POST);

   ?>
   </pre>
 </body>
</html>
